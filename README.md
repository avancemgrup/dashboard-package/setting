## Avannubo package Setting

Web setting

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/setting": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "url": "https://gitlab.com/avancemgrup/dashboard-package/setting.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Setting\SettingServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Setting\Seeds\PermissionSettingSeeder`
