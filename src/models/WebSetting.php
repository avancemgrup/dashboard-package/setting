<?php

namespace Avannubo\Setting\Models;

use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model{
    protected $fillable = [
        'id',
        'site_name',
        'description',
        'email',
        'telephone',
        'telephone_prefix',
        'fax_prefix',
        'fax',
        'address',
        'facebook',
        'twitter',
        'google',
        'youtube',
        'address_web_url',
    ];
}
