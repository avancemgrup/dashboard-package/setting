<?php

namespace Avannubo\Setting\Controllers;

use Avannubo\Setting\Models\WebSetting;
use Illuminate\Http\Request;
use Session;
use App\Http\Controllers\Controller;


class WebSettingController extends Controller {

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show the index view
     */
    public function  index(){
        $websetting = WebSetting::first();
        if ($websetting) {
            return $this->edit();
        } else {
            return $this->create();
        }

//        return view('setting::index', compact('websetting'));
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method validates the existence of the information in the database,
     * if it exists it redirects a web-setting else show the form for insert the information
     */
    public function create(){
        if(WebSetting::first()){
            return redirect('setting');
        }
        return view('setting::form');
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it inserts it into the database
     * else Shows error
     *
     */
    public function store(Request $request){
        if( WebSetting::first()){
            return redirect()->route('setting')->with('error','Resultado no encontrando');
        }
        // Validation
        $rules = [
            'site_name' => 'required|Max:255',
            'description' => 'required|Max:400',
            'email' => 'required|email|Max:255',
            'telephone_prefix' => 'nullable|Max:255',
            'telephone' => 'nullable|Max:14',
            'fax_prefix' => 'nullable|Max:255',
            'fax' => 'nullable|Max:14',
            'address' => 'nullable|Max:255',
            'facebook' => 'nullable|url|Max:255',
            'twitter' => 'nullable|url|Max:255',
            'google' => 'nullable|url|Max:255',
            'youtube' => 'nullable|url|Max:255',
            'address_web_url' => 'required|url|Max:255'
        ];

        $this->validate($request, $rules);

        ///Add information
        $websetting = new WebSetting([
            'site_name' => $request->input('site_name'),
            'description' => $request->input('description'),
            'email' => $request->input('email'),
            'telephone_prefix' => $request->input('telephone_prefix'),
            'telephone' => $request->input('telephone'),
            'fax_prefix' => $request->input('fax_prefix'),
            'fax' => $request->input('fax'),
            'address' => $request->input('address'),
            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'google' => $request->input('google'),
            'youtube' => $request->input('youtube'),
            'address_web_url' => $request->input('address_web_url'),
        ]);

        $env_file = base_path('.env');
        if (file_exists($env_file)) {
            $this->updateEnv($env_file, 'app.name', 'APP_NAME', $request->input('site_name'));
            $this->updateEnv($env_file, 'app.url', 'APP_URL', $request->input('address_web_url'));
        }

        if($websetting->save()){
            return redirect()->route('setting')->with(
                'message','Ajuste creado correctamente'
            );
        }else{
            return redirect()->route('setting')->with(
                'error','Ajuste no creado correctamente,intentelo mas tarde'
            );
        }

    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the information in the database,
     * if it exists it redirects a web-setting else show the form for update the information
     */
    public function edit(){
        $websetting = WebSetting::first();
        if($websetting){
            return view('setting::updateForm',compact('websetting'));
        }
        return redirect()->route('setting')->with( 'error','Ajuste no encontrado');

    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it update it into the database
     * else Shows error
     */
    public function update(Request $request){
        // Validation
        $websetting = WebSetting::first();

        if($websetting){
            $rules = [
                'site_name' => 'required|Max:255',
                'description' => 'required|Max:400',
                'email' => 'required|email|Max:255',
                'telephone_prefix' => 'nullable|Max:255',
                'telephone' => 'nullable|Max:14',
                'fax_prefix' => 'nullable|Max:255',
                'fax' => 'nullable|Max:14',
                'address' => 'nullable|Max:255',
                'facebook' => 'nullable|url|Max:255',
                'twitter' => 'nullable|url|Max:255',
                'google' => 'nullable|url|Max:255',
                'youtube' => 'nullable|url|Max:255',
                'address_web_url' => 'required|url|Max:255'
            ];
            $this->validate($request, $rules);

            $env_file = base_path('.env');
            if (file_exists($env_file)) {
                $this->updateEnv($env_file, 'app.name', 'APP_NAME', $request->input('site_name'));
                $this->updateEnv($env_file, 'app.url', 'APP_URL', $request->input('address_web_url'));
            }

            $websetting->site_name = $request->input('site_name');
            $websetting->description = $request->input('description');
            $websetting->email = $request->input('email');
            $websetting->telephone_prefix = $request->input('telephone_prefix');
            $websetting->telephone = $request->input('telephone');
            $websetting->fax_prefix = $request->input('fax_prefix');
            $websetting->fax = $request->input('fax');
            $websetting->address = $request->input('address');
            $websetting->facebook = $request->input('facebook');
            $websetting->twitter = $request->input('twitter');
            $websetting->google = $request->input('google');
            $websetting->youtube = $request->input('youtube');
            $websetting->address_web_url = $request->input('address_web_url');

            if($websetting->update()){
                return redirect()->route('setting')->with(
                    'message', 'Ajuste actualizado correctamente'
                );
            }else{
                return redirect()->route('setting')->with(
                    'error', 'Ajuste no actualizado correctamente'
                );
            }
        }
        return redirect()->route('setting')->with('error', 'Ajuste no encontrado');
    }

    private function updateEnv($env_file, $config, $key, $value) {
        file_put_contents($env_file,
            str_replace(
                $key . '="' . config($config) . '"',
                $key . '="' . $value . '"',
                file_get_contents($env_file)
            )
        );
    }
}