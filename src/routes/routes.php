<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {
    Route::get('setting',[
        'as' => 'setting',
        'uses' => '\Avannubo\Setting\Controllers\WebSettingController@index',
        'middleware' => ['permission:setting.view|setting.create|setting.edit']
    ]);
    Route::get('setting/add',[
        'as' => 'setting-add',
        'uses' => '\Avannubo\Setting\Controllers\WebSettingController@create',
        'middleware' => ['permission:setting.create']
    ]);
    Route::get('setting/edit',[
        'as' => 'setting-edit',
        'uses' => '\Avannubo\Setting\Controllers\WebSettingController@edit',
        'middleware' => ['permission:setting.edit']
    ]);
    Route::post('setting/add',[
        'as' => 'setting-add',
        'uses' => '\Avannubo\Setting\Controllers\WebSettingController@store',
        'middleware' => ['permission:setting.create']
    ]);
    Route::put('setting',[
        'as' => 'setting-edit-put',
        'uses' => '\Avannubo\Setting\Controllers\WebSettingController@update',
        'middleware' => ['permission:setting.edit']
    ]);
    
});