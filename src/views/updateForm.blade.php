@extends('layouts.administration.master')

@section('site-title')
    Setting
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Ajustes</h2>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'setting-edit-put', 'method'=>'PUT', 'autocomplete' => 'off')) !!}
                <div class="form-group {{ $errors->has('site_name') ? 'has-error' : '' }}">
                    <label for="site_name">Nombre web</label>
                    {!! Form::text('site_name', $websetting->site_name, array('placeholder' => 'Sitename','class' => 'form-control', 'id' => 'site_name')) !!}
                    <span class="text-danger">{{ $errors->first('site_name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::text('description', $websetting->description, array('placeholder' => 'Descripción','class' => 'form-control', 'id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('emai') ? 'has-error' : '' }}">
                    <label for="email">Email</label>
                    {!! Form::text('email', $websetting->email, array('placeholder' => 'Email','class' => 'form-control', 'id' => 'email')) !!}
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address_web_url">Web url</label>
                    {!! Form::url('address_web_url', $websetting->address_web_url, array('placeholder' => 'Web url','class' => 'form-control', 'id' => 'address_web_url')) !!}
                    <span class="text-danger">{{ $errors->first('address_web_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Direccion</label>
                    {!! Form::text('address', $websetting->address, array('placeholder' => 'Direccion','class' => 'form-control', 'id' => 'address')) !!}
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
                <div class="form-group {{ $errors->has('telephone_prefix') ? 'has-error' : '' }}">
                    <label for="telephone">Prefix de telefono</label>
                    {!! Form::text('telephone_prefix', $websetting->telephone_prefix, array('placeholder' => 'Telephone prefix','class' => 'form-control', 'id' => 'telephone_prefix')) !!}
                    <span class="text-danger">{{ $errors->first('telephone_prefix') }}</span>
                </div>
                <div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
                    <label for="telephone">Telefono</label>
                    {!! Form::text('telephone', $websetting->telephone, array('placeholder' => 'Telephone','class' => 'form-control', 'id' => 'telephone')) !!}
                    <span class="text-danger">{{ $errors->first('telephone') }}</span>
                </div>
                <div class="form-group {{ $errors->has('fax_prefix') ? 'has-error' : '' }}">
                    <label for="fax">Prefijo de fax</label>
                    {!! Form::text('fax_prefix', $websetting->fax_prefix, array('placeholder' => 'Email','class' => 'form-control', 'id' => 'fax_prefix')) !!}
                    <span class="text-danger">{{ $errors->first('fax_prefix') }}</span>
                </div>
                <div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                    <label for="fax">Fax</label>
                    {!! Form::text('fax', $websetting->fax, array('placeholder' => 'Fax','class' => 'form-control', 'id' => 'fax')) !!}
                    <span class="text-danger">{{ $errors->first('fax') }}</span>
                </div>
                <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                    <label for="facebook">Facebook url</label>
                    {!! Form::text('facebook', $websetting->facebook, array('placeholder' => 'Facebook url','class' => 'form-control', 'id' => 'facebook')) !!}
                    <span class="text-danger">{{ $errors->first('facebook') }}</span>
                </div>
                <div class="form-group {{ $errors->has('twitter') ? 'has-error' : '' }}">
                    <label for="twitter">Twitter url</label>
                    {!! Form::text('twitter', $websetting->twitter, array('placeholder' => 'Twitter url','class' => 'form-control', 'id' => 'twitter')) !!}
                    <span class="text-danger">{{ $errors->first('twitter') }}</span>
                </div>
                <div class="form-group {{ $errors->has('google') ? 'has-error' : '' }}">
                    <label for="google">Goole url</label>
                    {!! Form::text('google', $websetting->google, array('placeholder' => 'Goole url','class' => 'form-control', 'id' => 'google')) !!}
                    <span class="text-danger">{{ $errors->first('google') }}</span>
                </div>
                <div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
                    <label for="youtube">Youtube url</label>
                    {!! Form::text('youtube', $websetting->youtube, array('placeholder' => 'Youtube url','class' => 'form-control', 'id' => 'youtube')) !!}
                    <span class="text-danger">{{ $errors->first('youtube') }}</span>
                </div>
                @permission('setting.edit')
                <button type="submit" class="btn btn-success">
                   Actualizar
                </button>
                @endpermission
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
