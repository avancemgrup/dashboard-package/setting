@extends('layouts.administration.master')

@section('site-title')
    Setting
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nuevo Ajustes</h2>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'setting-add', 'method'=>'POST', 'autocomplete' => 'off')) !!}
                <div class="form-group {{ $errors->has('site_name') ? 'has-error' : '' }}">
                    <label for="site_name">Nombre del sitio web</label>
                    {!! Form::text('site_name', null, array('placeholder' => 'Nombre de web','class' => 'form-control', 'id' => 'site_name')) !!}
                    <span class="text-danger">{{ $errors->first('site_name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::text('description', null, array('placeholder' => 'Descripción','class' => 'form-control', 'id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('emai') ? 'has-error' : '' }}">
                    <label for="email">Email:</label>
                    {!! Form::email('email', null, array('placeholder' => 'Email','class' => 'form-control', 'id' => 'email')) !!}
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
                 <div class="form-group {{ $errors->has('address_web_url') ? 'has-error' : '' }}">
                    <label for="address_web_url">Web url</label>
                    {!! Form::url('address_web_url', null, array('placeholder' => 'Web url','class' => 'form-control', 'id' => 'address_web_url')) !!}
                    <span class="text-danger">{{ $errors->first('address_web_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Dirección</label>
                    {!! Form::text('address', null, array('placeholder' => 'Dirección','class' => 'form-control', 'id' => 'address')) !!}
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
                <div class="form-group {{ $errors->has('telephone_prefix') ? 'has-error' : '' }}">
                    <label for="telephone">Prefijo de teléfono</label>
                    {!! Form::text('telephone_prefix', null, array('placeholder' => 'Prefijo de teléfono','class' => 'form-control', 'id' => 'telephone_prefix')) !!}
                    <span class="text-danger">{{ $errors->first('telephone_prefix') }}</span>
                </div>
                <div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
                    <label for="telephone">Teléfono</label>
                    {!! Form::text('telephone', null, array('placeholder' => 'Teléfono','class' => 'form-control', 'id' => 'telephone')) !!}
                    <span class="text-danger">{{ $errors->first('telephone') }}</span>
                </div>
                <div class="form-group {{ $errors->has('fax_prefix') ? 'has-error' : '' }}">
                    <label for="fax">Prefijo de fax</label>
                    {!! Form::text('fax_prefix', null, array('placeholder' => 'Prefijo de fax','class' => 'form-control', 'id' => 'fax_prefix')) !!}
                    <span class="text-danger">{{ $errors->first('fax_prefix') }}</span>
                </div>
                <div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                    <label for="fax">Fax</label>
                    {!! Form::text('fax', null, array('placeholder' => 'Fax','class' => 'form-control', 'id' => 'fax')) !!}
                    <span class="text-danger">{{ $errors->first('fax') }}</span>
                </div>

                <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                    <label for="facebook">Facebook url</label>
                    {!! Form::url('facebook', null, array('placeholder' => 'Facebook url','class' => 'form-control', 'id' => 'facebook')) !!}
                    <span class="text-danger">{{ $errors->first('facebook') }}</span>
                </div>
                <div class="form-group {{ $errors->has('twitter') ? 'has-error' : '' }}">
                    <label for="twitter">Twitter url</label>
                    {!! Form::url('twitter', null, array('placeholder' => 'Twitter url','class' => 'form-control', 'id' => 'twitter')) !!}
                    <span class="text-danger">{{ $errors->first('twitter') }}</span>
                </div>
                <div class="form-group {{ $errors->has('google') ? 'has-error' : '' }}">
                    <label for="google">Goole url</label>
                    {!! Form::url('google', null, array('placeholder' => 'Goole url','class' => 'form-control', 'id' => 'google')) !!}
                    <span class="text-danger">{{ $errors->first('google') }}</span>
                </div>
                <div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
                    <label for="youtube">Youtube url</label>
                    {!! Form::url('youtube', null, array('placeholder' => 'Youtube url','class' => 'form-control', 'id' => 'youtube')) !!}
                    <span class="text-danger">{{ $errors->first('youtube') }}</span>
                </div>
                @permission('setting.create')
                <button type="submit" class="btn btn-success">
                  Crear
                </button>
                @endpermission
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

