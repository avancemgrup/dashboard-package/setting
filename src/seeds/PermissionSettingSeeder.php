<?php

namespace Avannubo\Setting\Seeds;

use Illuminate\Database\Seeder;
use App\Permission;
class PermissionSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'setting.view',
            'display_name' => 'setting  view',
            'description' => 'Ver ajustes'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'setting.create',
            'display_name' => 'setting create',
            'description' => 'Crear ajustes'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'setting.edit',
            'display_name' => 'setting edit',
            'description' => 'Editar ajustes'
        ]);
        $person->save();

    }
}
