<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSocialNetworksNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('web_settings', function (Blueprint $table) {
            $table->string('facebook')->nullable()->after('address')->change();
            $table->string('twitter')->nullable()->change('facebook');
            $table->string('google')->nullable()->change('twitter');
            $table->string('youtube')->nullable()->change('youtube');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
