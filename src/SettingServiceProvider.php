<?php

namespace Avannubo\Setting;

use Avannubo\Setting\Models\WebSetting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/WebSetting.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'setting');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        try {
            $webSeting = WebSetting::orderBy('created_at', 'desc')->first();
            View::share('webSetting', $webSeting);
        } catch (\Exception $e) {
            return [];
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->make('Avannubo\Setting\Controllers\WebSettingController');
    }
}
